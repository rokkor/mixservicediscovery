package me.pixka.mixdiscovery

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class MixdiscoveryApplication

fun main(args: Array<String>) {
	runApplication<MixdiscoveryApplication>(*args)
}
